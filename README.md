# Konami Builder Widget

## Input

- Swipe `Up`, `Up`, `Down`, `Down`, `Left`, `Right`, `Left`, `Right`,
- Double tap the screen.
- Double tap bottom right (25x25 transparent container) toggle gesture detector

## Output

![](https://i.ibb.co/Fb2t8xp/Screenshot-2020-10-25-at-14-40-26.jpg) 

## Expected Usage

```dart
class _MyAppState extends State<MyApp> {
  List<KonamiMenuItem> _menu;
  String _selected;

  @override
  void initState() {
    super.initState();

    // 1. create a menu with string and items
    _menu = [
      KonamiMenuItem('Foo', Foo()),
      KonamiMenuItem('Bar', Bar()),
      KonamiMenuItem('Baz', Baz()),
    ];

    _selected = _menu[0].id;
  }

  void updateValue(value) {
    setState(() {
      _selected = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Konami Builder',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      // 2. configure settings
      home: KonamiBuilder(
        title: 'Secret Menu',
        selectors: _menu,
        child: _menu.firstWhere((item) => item.id == _selected).widget,
        onUpdatedValue: (value) => updateValue(value),
        codeRequired: true, // allow double click to show dialog
        debug: true, // print logs
      ),
    );
  }
}
```