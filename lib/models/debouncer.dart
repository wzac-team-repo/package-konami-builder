import 'dart:async';
import 'package:flutter/material.dart';

/**
 * Debouncer
 * Credit to: https://stackoverflow.com/a/55119208/6212975
 */
class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (_timer != null) {
      _timer.cancel();
    }

    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
