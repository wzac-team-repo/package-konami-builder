import 'package:flutter/material.dart';

/**
 * Konami Menu Item
 */
class KonamiMenuItem {
  final String id;
  final Widget widget;

  KonamiMenuItem(this.id, this.widget);
}
