import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'models/debouncer.dart';
import 'models/konami_menu_item.dart';

/**
 * This service allow you to go to different pages with secret gesture
 */
class KonamiBuilder extends StatefulWidget {
  final Widget child;
  final Function onUpdatedValue;
  final List<KonamiMenuItem> selectors;
  final String title;
  final bool codeRequired;
  final bool debug;
  final String konamiCode;

  const KonamiBuilder({
    Key key,
    this.child,
    this.onUpdatedValue,
    this.selectors,
    this.title,
    this.konamiCode = 'UpUpDownDownLeftRightLeftRight',
    this.codeRequired = true,
    this.debug = false,
  }) : super(key: key);

  @override
  _KonamiBuilderState createState() => _KonamiBuilderState();
}

class _KonamiBuilderState extends State<KonamiBuilder> {
  final _debounce = Debouncer(milliseconds: 100);
  bool enableGesture = true;

  /**
   * ========== Build ==========
   */
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      enableGesture
          ? GestureDetector(
              onVerticalDragUpdate: (details) => onSwipeVertical(details),
              onHorizontalDragUpdate: (details) => onSwipeHorizontal(details),
              onDoubleTap: () => launch(),
              child: widget.child,
            )
          : widget.child,
      Positioned(
        bottom: 0,
        right: 0,
        child: GestureDetector(
          onDoubleTap: () {
            setState(() {
              enableGesture = !enableGesture;
              print("enableGesture: $enableGesture");
            });
          },
          child: Container(
            width: 24,
            height: 24,
            color: enableGesture ? Colors.green : Colors.red,
          ),
        ),
      )
    ]);
  }

  /**
   * ========== Business Logic ==========
   */
  String _konamiCode;
  String _attempt;

  @override
  void initState() {
    super.initState();
    _konamiCode = widget.konamiCode;
    _attempt = '';
  }

  /**
   * concatenate attempted values
   */
  void attempt(value) {
    _attempt = _attempt + value;
    if (widget.debug == true) print(_attempt);
  }

  /**
   * Resuet attempted values
   */
  void reset() {
    setState(() {
      _attempt = '';
    });
  }

  void onSwipeVertical(details) {
    _debounce.run(() {
      if (details.delta.dy < 0) {
        attempt('Up');
      }
      if (details.delta.dy > 0) {
        attempt("Down");
      }
    });
  }

  void onSwipeHorizontal(details) {
    _debounce.run(() {
      if (details.delta.dx < 0) {
        attempt("Left");
      }
      if (details.delta.dx > 0) {
        attempt("Right");
      }
    });
  }

  /**
   * Launch
   */
  void launch() {
    if (_konamiCode == _attempt || widget.codeRequired == false) {
      onPasswordCorrect();
    }
    reset();
  }

  /**
   * Event:: when attempted password matches konami code
   */
  onPasswordCorrect() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        elevation: 10.0,
        title: Text(widget.title),
        content: SingleChildScrollView(
          child: Column(
            children: widget.selectors.map((item) {
              String index = (widget.selectors.indexOf(item) + 1).toString();

              return GestureDetector(
                onTap: () => widget.onUpdatedValue(item.id),
                child: Card(
                  child: ListTile(
                    leading: Text(index),
                    title: Text(item.id),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
        actions: <Widget>[
          CupertinoButton(
            child: Text('Close'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
